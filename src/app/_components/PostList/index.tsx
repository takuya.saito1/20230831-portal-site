import { Blog } from "@/libs/microcms";
import PostListItem from "../PostListItem";
import styles from "./index.module.css";

type Props = {
  posts?: Blog[];
  href: string;
};

export default function PostList({ posts, href }: Props) {
  if (!posts) {
    return null;
  }
  if (posts.length === 0) {
    return <p>記事がありません。</p>;
  }
  return (
    <ul className={styles.postList}>
      {posts.map((post) => (
        <PostListItem key={post.id} post={post} href={href} />
      ))}
    </ul>
  );
}
