import Link from "next/link";
import Image from "next/image";
import { Blog } from "@/libs/microcms";
import styles from "./index.module.css";
import PublishedDate from "../Date";

type Props = {
  post: Blog;
  href: string;
};

export default function PostListItem({ post, href }: Props) {
  return (
    <li className={styles.list}>
      <Link href={`${href}${post.id}`} className={styles.link}>
        {post.eyecatch ? (
          <Image
            src={post.eyecatch?.url}
            alt=""
            className={styles.image}
            width={post.eyecatch?.width}
            height={post.eyecatch?.height}
          />
        ) : (
          <Image
            className={styles.image}
            src="/no-image.png"
            alt="No Image"
            width={1200}
            height={630}
          />
        )}
        <dl className={styles.content}>
          <dt className={styles.title}>{post.title}</dt>
          <dd className={styles.meta}>
            <PublishedDate date={post.publishedAt || post.createdAt} />
          </dd>
        </dl>
      </Link>
    </li>
  );
}
