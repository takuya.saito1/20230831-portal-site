import Link from "next/link";
import { getArticles } from "../../libs/newt";

export default async function Page() {
  const articles = await getArticles();

  // ページの生成された時間を取得
  const time = new Date().toLocaleString();

  if (!articles || articles.length === 0) {
    return <h1>No articles</h1>;
  }

  return (
    <div>
      <h1>Newt - Page</h1>
      <h2>{time}</h2>
      <ul>
        {articles.map((article) => {
          return (
            <li key={article._id}>
              <Link href={`newt/${article.slug}`}>{article.title}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
