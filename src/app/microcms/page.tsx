import ButtonLink from "@/app/_components/ButtonLink";
import styles from "./page.module.css";

export default function Page() {
  return (
    <div className={styles.container}>
      <ButtonLink href="/microcms/dynamic">microCMS - dynamic</ButtonLink>
      <ButtonLink href="/microcms/static">microCMS - static</ButtonLink>
    </div>
  );
}
