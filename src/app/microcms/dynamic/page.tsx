import { getList } from "../../../libs/microcms";
import PostList from "@/app/_components/PostList";

// キャッシュを利用しない
export const revalidate = 0;

export default async function DynamicPage() {
  const { contents } = await getList();

  // ページの生成された時間を取得
  const time = new Date().toLocaleString();

  if (!contents || contents.length === 0) {
    return <h1>No contents</h1>;
  }

  return (
    <div>
      <h1>microCMS - Dynamic Page</h1>
      <h2>{time}</h2>
      <PostList posts={contents} href={"/microcms/dynamic/"} />
    </div>
  );
}
