# 公開環境

## App - Vercel

- https://20230831-portal-site.vercel.app/

## ヘッドレスCMS - microCMS, Newt

- [microCMS](https://csd96ggmh0.microcms.io/apis)
- [Newt](https://app.newt.so/20230831-portal-site-newt/apps/blog)

# 仕様技術

- [React](https://react.dev/)
- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Tailwind CSS](https://tailwindcss.com/)
